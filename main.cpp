#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <time.h>
#include <cstdlib>
#define Dot_ran 254
#define MAX 100
#define LEN 0
#define XUONG 1
#define TRAI 2
#define PHAI 3
#define TUONG_TREN 3
#define TUONG_DUOI 23
#define TUONG_TRAI 3
#define TUONG_PHAI 70
#define GREEN 10
using namespace std;

struct Point{
    int x,y;
};
Point ViTriCuoi;
Point con_ran[MAX];
int soDot=3;
void KhoitaoRan(){
    con_ran[0].x=12;
    con_ran[1].x=11;
    con_ran[2].x=10;
    con_ran[0].y=con_ran[1].y=con_ran[2].y=5;
}
void gotoXY (int x, int y)
{
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}
void HienThiRan(Point dotCuoiCu ){
    for(int i=0; i<soDot ; i++){
        gotoXY(con_ran[i].x,con_ran[i].y);
        cout << (char)Dot_ran;
    }
    gotoXY(dotCuoiCu.x,dotCuoiCu.y);
    cout << " " ;
}
Point dichuyen(int huong){
    Point dotCuoiCu=con_ran[soDot-1];
    for(int i=soDot-1; i>=1; i--){
        con_ran[i]=con_ran[i-1];
    }
    if(huong==LEN) con_ran[0].y--;
    else if (huong==XUONG) con_ran[0].y++;
    else if (huong==TRAI) con_ran[0].x--;
    else if (huong==PHAI) con_ran[0].x++;

    else if(huong==XUONG) con_ran[0].y++;
    else if(huong==TRAI) con_ran[0].x--;
    else if(huong==PHAI) con_ran[0].x++;
    return dotCuoiCu;

}
void batsukien(int &huong){
    char key;
    if(kbhit()){
        key=getch();
        if(key=='w'){
            huong=LEN;
        }
        else if(key=='s'){
            huong=XUONG;
        }
        else if(key=='a'){
            huong=TRAI;
        }
        else if(key== 'd'){
            huong=PHAI;
        }
    }
}
void ve_tuong(){
    for(int x=TUONG_TRAI; x<=TUONG_PHAI; x++){
        gotoXY(x,TUONG_TREN);
        cout << (char)220;
    }
    for(int y=TUONG_TREN+1; y<=TUONG_DUOI; y++){
        gotoXY(TUONG_TRAI,y);
        cout << (char)221;
    }
    for(int x=TUONG_TRAI; x<=TUONG_PHAI; x++){
        gotoXY(x,TUONG_DUOI);
        cout << (char)223;
    }
    for(int y=TUONG_TREN+1; y<=TUONG_DUOI-1; y++){
        gotoXY(TUONG_PHAI,y);
        cout << (char)222;
    }
}
bool KiemTraThua()
{
    if(con_ran[0].y==TUONG_TREN || con_ran[0].y == TUONG_DUOI )
        return true;
    if(con_ran[0].x==TUONG_TRAI || con_ran[0].x == TUONG_PHAI)
        return true;
    for(int i=1; i<soDot; i++){
        if(con_ran[0].x==con_ran[i].x && con_ran[0].y==con_ran[i].y )
            return true;
    }
    return false;
}
void xu_ly_thua(){
    Sleep(2000);
    system("cls");
    cout << "Game over.\n";
}
Point HienThiMoi(){
    Point p;
    int x,y;
    srand(time(NULL));
    x=TUONG_TRAI +1 + rand()%(TUONG_PHAI- TUONG_TRAI - 1 );
    y=TUONG_TREN +1 + rand()% (TUONG_DUOI- TUONG_TREN -1);
    gotoXY(x,y);
    cout <<  "o";
    p.x=x;
    p.y=y;
    return p;
}
bool KiemTraDaAnMoi(Point moi){
    if(moi.x==con_ran[0].x && moi.y==con_ran[0].y) return true;
    else return false;
}
void ThemDot(){
    con_ran[soDot]=con_ran[soDot-1];
    soDot++;
}
int tocdo(int diem){
    if(diem <10) return 100;
    else if( 10<=diem && diem < 20) return 80;
    else if(20<=diem && diem <30) return 60;
    else if(30<=diem && diem <40 ) return 40;
    else if( 40<=diem && diem < 50) return 20; }
bool KiemTraDiChuyen(int huong1, int huong2){
    if(huong1==LEN && huong2==XUONG) return false;
    if(huong1==XUONG && huong2==LEN) return false;
    if(huong1==TRAI && huong2==PHAI) return false;
    if(huong1==PHAI && huong2==TRAI) return false;
    return true;
}

int main()
{
    Point dotCuoiCu;
    int diem=0;
    int huong=PHAI,huongCu=PHAI;
    KhoitaoRan();
    ve_tuong();
    Point moi = HienThiMoi();
    gotoXY(TUONG_PHAI + 3,TUONG_TREN);
    cout << "Diem : " << diem;
   while(1){
    batsukien(huong);
    if(KiemTraDiChuyen(huongCu,huong)){
        dotCuoiCu=dichuyen(huong);
        huongCu=huong;
    }
    else {
        dotCuoiCu=dichuyen(huongCu);
    }
    HienThiRan(dotCuoiCu);
    if(KiemTraDaAnMoi(moi)){
        moi=HienThiMoi();
        ThemDot();
        diem++;
        gotoXY(TUONG_PHAI + 3,TUONG_TREN);
        cout << "Diem : " << diem;
    }
    if(KiemTraThua()){
        break;
    }
    Sleep(tocdo(diem));
   }
   xu_ly_thua();
    return 0;
}



